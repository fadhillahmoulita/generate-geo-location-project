package google.here;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class GeoLocSearchJava {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String csvFile = "D:\\Temp\\GeoLoc\\geolocsearch2.csv";
        List<Map> listReadCsv = new ArrayList<Map>();
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            int i = 0;
            while ((line = reader.readNext()) != null) {
                i =i+1;
                if (i==1) continue;
                Map mapData = new HashMap();
                mapData.put("hotelId", line[0]);
                mapData.put("hotelName", line[1]);
                mapData.put("latitude", line[2]);
                mapData.put("longitude", line[3]);
                listReadCsv.add(mapData);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Map> lsMap = new ArrayList<Map>();
        JSONParser parser = new JSONParser();
        for (Map mapReadCsv : listReadCsv) {
            try {
                String tempLat = mapReadCsv.get("latitude").toString() +"," + mapReadCsv.get("longitude").toString() +"," + "1000";
                System.out.println("tempLat = " + tempLat);
                final String fooResourceUrl = "https://reverse.geocoder.api.here.com/6.2/reversegeocode.json?app_id=n9upjkjZM4KNtVTqIpzI&app_code=rQJQR3yKW-GSgOi2k-6PLw&mode=retrieveAreas&gen=9&prox=" + tempLat;
                System.out.println("fooResourceUrl = " + fooResourceUrl);
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);

                JSONObject responseInfo = (JSONObject)parser.parse(response.getBody());
                JSONObject jspnResponse = (JSONObject) responseInfo.get("Response");
                JSONArray arrView = (JSONArray) jspnResponse.get("View");
                JSONObject  objView = (JSONObject) arrView.get(0);
                JSONArray arrRess = (JSONArray) objView.get("Result");
                JSONObject objRess = (JSONObject) arrRess.get(0);
                JSONObject objLoc = (JSONObject)  objRess.get("Location");
                JSONObject objAddr = (JSONObject)  objLoc.get("Address");
                System.out.println(objAddr);
                Map mapData = new HashMap();
                mapData.put("objAddr", objLoc.get("Address"));
                mapData.put("Label", objAddr.get("Label"));
                mapData.put("Country", objAddr.get("Country"));
                mapData.put("PostalCode", objAddr.get("PostalCode"));
                mapData.put("City", objAddr.get("City"));
                mapData.put("Subdistrict", objAddr.get("Subdistrict"));
                mapData.put("County", objAddr.get("County"));
                mapData.put("District", objAddr.get("District"));

                mapData.put("hotelId", mapReadCsv.get("hotelId"));
                mapData.put("hotelName",mapReadCsv.get("hotelName"));
                mapData.put("latitude",mapReadCsv.get("latitude"));
                mapData.put("longitude",mapReadCsv.get("longitude"));

                lsMap.add(mapData);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try{
            CSVWriter csvWriter = new CSVWriter(new FileWriter("D:\\Temp\\GeoLoc\\resultEan.csv"));
            for (Map maps : lsMap) {
                csvWriter.writeNext(new String[]{ maps.get("hotelId") +"," + maps.get("hotelName") +","  + maps.get("latitude") +"," + maps.get("longitude")
                        +"," +  maps.get("Country")  +","+ maps.get("County")  +","+ maps.get("City")
                        +","+ maps.get("District") +","+ maps.get("Subdistrict") });
            }
            csvWriter.close();
        }catch (Exception e) {
            // TODO: handle exception
        }

        try{
            CSVWriter csvWriter = new CSVWriter(new FileWriter("D:\\Temp\\GeoLoc\\resultEanBackup.csv"));
            for (Map maps : lsMap) {
                csvWriter.writeNext(new String[]{ maps.get("hotelId") +"," + maps.get("hotelName") +"," + maps.get("partner") +"," + maps.get("latitude") +"," + maps.get("longitude") +"," + maps.get("objAddr") });
            }
            csvWriter.close();
        }catch (Exception e) {
            // TODO: handle exception
        }

    }

}
